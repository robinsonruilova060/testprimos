import unittest
from primos import *

class TestPrime(unittest.TestCase):
    def test_prime(self):
        self.assertEqual(primo(7), True)


if __name__ == '__main__':
    unittest.main()